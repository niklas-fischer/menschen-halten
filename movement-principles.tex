% !TEX root = menschen-halten.tex
\section{Die Prinzipien der Bewegung}
\label{movement-principles}

Damit Sie verstehen, wie sich Ihr Mensch anpasst, wenn er sich bewegt und Dinge lernt, führen wir Sie in die Theorie ein. Im Bereich der Bewegung gibt es Prinzipien, die ausschlaggebend sind, um gesund und leistungsfähig zu werden\footnote{Gesundheit impliziert Leistungsfähigkeit und umgekehrt. Kein neuer Rekord im Sprinten, wenn Ihr Mensch im Krankenhaus liegt.} Das, was wir Ihnen vorstellen, ist Wissen, dass wir aus dem Körper extrahiert haben. Denn es sind Prinzipien, nach der die Natur funktioniert. Die Natur von Organismen geht nach denselben Prinzipien vor und deshalb sind sie universell übertragbar. Wenn Sie sie verinnerlicht haben, werden Sie ihnen häufiger begegnen, als Ihnen vielleicht lieb ist. 

\subsection*{Die Hantelstrategie}

Eine Hantel\footnote{Kurz, mittel, lang -- egal! Hauptsache Hantel.} ist eine lange, schwere Stange, an deren beiden Enden schwere Gewichte sind. Stellen Sie sich vor, wie sie aussieht. In der Mitte eine schmale Stange, an den Enden überbordende Gewichte, die riesig und schwer aussehen. Das soll uns als Sinnbild, als Metapher für ein Prinzip der Natur gelten. Was bringt Ihrem Menschen diese Strategie? Sie ist die Quintessenz der zuvor genannten Prinzipien, eine Vereinfachung. 

Sich nur in der Mitte des eigenen Leistungsbereichs aufzuhalten ist nicht falsch, nur wenig zielführend. Dabei handelt es sich um die Art der Bewegung, die weder besonders intensiv noch anspruchslos ist. Sie ist irgendwo dazwischen. Sie ist lasch, nicht hart. Menschen gehen gerne solchen Tätigkeiten nach, weil sie nicht besonders viel Wille benötigt und weil sie sich wie Sport anfühlt. Joggen ist ein solches Beispiel. Menschen gehen in extra gekauften Klamotten die immerselbe Runde drehen. Keine Verbesserung, immer dasselbe in derselben Zeit. Das ist nicht nur langweilig, sondern potenziell schädlich. Wenn Ihr Mensch schon die Willenkraft aufgebracht hat, warum sich mit halbgaren Lösungen zufrieden geben, sich der Gefahr des Einschleichens von Fehlern oder Unteranpassung auszusetzen?

Betrachten wir Bewegung als eine Linie. Am einen Ende befindet sich der Bereich, in dem wir uns oft bewegen, aber nur sehr geringe Reize haben -- zu gehen ist ein solches Beispiel. Am anderen Ende befindet sich die Extrembelastung. Sie findet nur sehr selten statt, aber ihre Reize sind enorm -- dazu zählt beispielsweise ein Sprint\footnote{Typischerweise ist das mit einer Flucht gleichzusetzen und deshalb ist der Mensch darauf auch adaptiert.}. Zwischen den beiden Enden ist die Bewegung angesiedelt, die weder besonders oft noch besonders intensiv ist und das ist der Bereich, den es zu vermeiden gilt.
Sich nur an den Enden der Stange aufzuhalten ist die beste Herangehensweise.
Die Extrembelastung reizt den Körper enorm. Nach einer solchen Belastung sind Menschen kaputt, nicht mehr ansprechbar, wollen nur noch herumliegen. Durch die nur kurze Belastung, von insgesamt über die Flucht vor einem Tier dauert die Belastung nur wenige Minuten, wenn überhaupt. Der Körper hält auch nicht länger durch. 
Das andere Ende ist der Teil, in dem sich der Mensch am besten im Alltag aufhält. Zu gehen ist wesentlich, aber auch aufzustehen, etwas von Hand zu kochen oder den Haushalt zu machen. Leicht, aber deutlich mehr als nichts zu tun. 
Das ist die Kraft der Hantel. Beides kombiniert lässt Ihren Menschen von den besten Enden profitieren. 

\subsection*{Mäßigung ist Macht -- Superkompensation}
\label{movement-supercompensation}

Kennen Sie den Begriff Antifragilität? Vereinfacht gesagt bedeutet es, dass bestimmte Organismen unter Einfluss von Stress nicht daran zerbrechen, sondern sich positiv anpassen (und damit überkompensieren). Der menschliche Körper ist so gemacht, dass er nach Setzen eines wirksamen Reizes vorübergehend Leistung verliert, sie nach und nach wieder aufbaut, und zwar über das ursprüngliche Niveau hinaus. Wenn Ihr Mensch immer einen Sack von Mehl schleppt, fällt ihm das in den ersten Wochen sehr schwer. Er wird ermüdet sein, den Sack kaum hochhieven können. Nach einem Wochenende der Ruhe und ohne des Schleppens von Mehlsäcken fällt ihm auf: \textit{Hui, das geht ja ganz leicht.} Plötzlich kann Ihr Mensch sich leicht wie eine Feder mit dem Mehlsack durch Raum und Zeit bewegen. Das ist Superkompensation. Superkompensation funktioniert nur, wenn Sie einen Reiz mit Erholung verbinden.
Was ist Yin ohne Yang, Sonne ohne Dunkelheit oder Kuchen ohne Sahne? Das eine kann ohne das andere nicht existieren und so verhält es sich auch mit dem Krafttraining und der Erholung davon. Schleppen Sie Ihren Menschen nicht jeden Tag ins Fitnessstudio. Das ermüdet ihn nur. Nur wer nach einer sportlichen Einheit anhand der anderen Prinzipien ausreichend ruht, er wird stärker. Die Ruhezeit sollte zwischen ein und zwei Tagen sein, bevor die Gewichte wieder der Gravitation trotzen dürfen. Das wichtigste Indiz der Bereitschaft für eine Trainingseinheit ist das dabei das eigene Körpergefühl.

\subsection*{Prinzip I: Wirksamkeit}

Gehen Sie von folgendem Gedanken aus: Ihr Mensch hebt jeden Tag einen Kieselstein fünfmal auf und legt ihn wieder hin. Wenn wir nur lange genug warten, wird er irgendwann so viel gehoben haben wie ein großer Felsklotz schwer ist\footnote{Und wenn wir ein paar Jahrhunderte Zeit haben, entspricht das gehobene Gewicht der Menge von einem Gebirge.}. Nett, weil Ihr Mensch ein bisschen Steine bewegt hat, aber unwirksam im Vergleich zum Anheben eines Felsklotzes. Allein einen riesigen Stein einmalig zu heben wird eine spezifische Anpassung erzeugen, die Ihren Menschen stärker macht. Das ist das Prinzip der Wirksamkeit. Der Reiz muss intensiv genug sein, dass sich Ihr Zweibeiner wirklich anstrengen muss. Aber nicht so schwer, dass er sich dabei verletzt oder eine ungesunde Körperhaltung einnimmt. Der Stein muss zu Ihrem Menschen passen. Ihr Mensch muss sich anstrengen, sonst wird sein Körper sich nicht anpassen.


\subsection*{Prinzip II: Spezifität}

Ihr Mensch will sich eine bestimmte Fähigkeit aneignen. Vielleicht will er Jonglieren lernen\footnote{Lustige Kreisbewegungen mit den Händen und drei oder mehr Bällen verursachen.}, einen einarmigen Handstand lernen oder die nervigen Nachbarn gezielt mit einem Blumentopf bewerfen. Jedes Hobby ist so legitim wie das andere\footnote{Solange niemand verletzt oder geschädigt wird, siehe vorheriges Beispiel.}. Wenn Ihr Mensch nun besonders gut mit dem Werfen von Blumentöpfen werden will, so bleibt ihm nichts anderes übrig, als genau das zu üben. Würde er stattdessen anfangen zu jonglieren, würde er vermutlich nicht besser Blumentöpfe werfen. Schachspieler bauen nicht ihre allgemeinen strategischen Fertigkeit aus. Wenn, dann besitzen Sie diese strategischen Fertigkeiten bereits und nutzen Sie im Schach, nicht umgekehrt. So verhält es sich mit sehr vielen Dingen. Das ist das Prinzip der Spezifität. Nur, wenn die spezifische Sache übt, wird besser darin. Ein Läufer geht in den Wald, um zu laufen. Ein Mensch hebt schwere Gewichte, um stärker zu werden. Das widerspricht nicht der Tatsache, dass sich Training von Kraft auch auf die Ausdauer auswirkt und umgekehrt, allerdings wirkt sich nur spezifisches Training direkt auf den trainierten Aspekt aus. 

\subsection*{Prinzip III: Progression}

Progression bedeutet Fortschritt. Ihr trainierender Mensch wird nur besser in etwas, indem er Fortschritt macht. Das wird beim Training erreicht. Durch mehr Wiederholungen, mehr Geschwindigkeit, mehr Sätze, mehr Gewicht, längerer Haltedauer oder schönerer Form. Es bedeutet, mehr Arbeit zu verrichten. Immer auf demselben Niveau zu bleiben ist Stillstand. Stillstand ist nicht nur Stillstand, sondern Rückschritt. Das heißt, dass Ihr Mensch immer ein klein wenig mehr machen sollte, was auch immer er lernen will. Hier eine Vokabel mehr, da drei Wiederholungen mehr Liegestütz. Seien Sie ihm ein guter Lehrer und bringen Sie ihm dieses Prinzip bei. Ohne Veränderung keine Veränderung. 


\subsection*{In die Tiefe}

Die kommenden drei Prinzipien sind nicht unbedingt notwendig zu wissen. Deshalb dürfen Sie, lieber Leser, auch diesen Abschnitt überschreiten, falls Sie das Buch bisher gelangweilt hat und sie kurz vor dem Einschlafen sind\footnote{Entschuldigung, das war passiv-aggressiv. Das Buch ist sehr gut, das wissen Sie ja bereits.}.

\subsubsection*{Prinzip V: Reihenfolge}

Der Ablauf des Trainings hat Einfluss auf die Auswirkungen. Ihr Mensch sollte mit Übungen beginnen, die ihn geistig anstrengen und welche koordinativ anspruchsvoll sind. Danach folgen die Übungen, die viel Stärke kosten und beendet wird ein Training mit ausdauerintensiven Einheiten und Dehnen. Der dahinterliegende Grund hängt mit den verfügbaren Ressourcen des menschlichen Körpers zusammen. Mit fortschreitendem Training erschöpft der Körper und der Geist verabschiedet sich langsam in den Ruhe(zu)stand. Dadurch sollten sie die Komplexität der Übungen mit zunehmenden Training senken, um Ihren Menschen optimal zu trainieren. Das Prinzip lässt sich auch leicht auf Training außerhalb von Sport ausdehnen. Um zu schreiben, mag sich eine technische Fingerübung zu Beginn anbieten. Danach folgt der Text, der Ihnen viel geistige Kraft abverlangt, weil er schwierig zu schreiben ist und am Ende kommen die Stellen, die sich einfach von den Fingern rattern lassen. 

\subsubsection*{Prinzip VI: Abwechslung}

Abwechslung ist nicht nur essenziell, um der Langeweile entgegenzuwirken. Sie ist notwendig, um besser zu werden. Wenn Ihr Mensch immer in derselben Umgebung übt, wird er niemals außerhalb dieses Rahmens besser werden. Wenn Ihr Mensch sich immer auf die gleiche Weise bewegt, wie soll er herausfinden, ob er nicht eine andere Bewegung besser kann oder verträgt? Wie soll er sich verändernden Umständen anpassen? Geben Sie Ihrem Menschen deshalb die Möglichkeit, seine Fertigkeit in einer anderen Umgebung, mit anderen Übung oder unter anderen Umständen zu üben. Bringen Sie Variation, Entropie und Chaos in sein Training. Die Natur ist kein Fitnessstudio mit der gleichen Beleuchtung, Temperatur und Gewichten, sondern ein sich fortwährend verändernder Schauplatz, denen sich Ihr Mensch immer wieder zu stellen hat. Variation verbessert die Breite der Fertigkeit und macht Ihren Menschen robuster -- geben Sie ihm doch die Chance, damit zu spielen. 

\subsubsection*{Prinzip VII: Individualität}

Ist Ihr Mensch schwächlich, hat einen krummen Rücken und trägt eine Brille mit metertiefen Fenstern? Wir empfehlen, dass Ihr Mensch kein Rugby-Spieler\footnote{Das ist eine Menschensportart, bei der ein eiförmiges Objekt rabiat durch die Gegend bewegt wird. Deren Spieler sind stark, schnell und selten kurzsichtig.} wird, sondern anhand seiner individuellen Bedürfnisse Sport betreibt. Das ist in Ordnung. Jeder fängt irgendwo an. Jeder Mensch wird mit verschiedenen, trainierbaren Eigenschaften geboren. Finden Sie heraus, was Ihr Mensch kann, worin er besonders gut ist und maximieren Sie seine Fertigkeiten. Merzen Sie Schwächen aus, wo auch immer Sie können. Die Grenzen sind nur da, wo es sich die Person steckt, aber anfangen muss Ihr Mensch schon selbst.

\subsection*{Ist Ihr Mensch körperlich gesund?}


Ist Ihr geliebter Zweibeiner topfit für jeden Sport? Der Test gibt Anhaltspunkte, ob Ihr Mensch noch Lücken oder Schwächen hat.

\begin{itemize}
    \item Hat Ihr Mensch Freude an Bewegung, macht beim Spaziergang zusätzlich Bewegungen wie Luftsprünge oder will eine Wand hochklettern? Will er beim Vorbeigehen an einem Spielplatz mit den kleinen Rackern zusammen klettern, hangeln oder auf allen Vieren laufen? 
    \item Ist Ihr Mensch unempfindlich gegenüber Hitze oder Kälte? Hält er eine kalte Dusche aus, ohne die Wand hochzugehen und schafft er es, mehr als zehn Minuten in einer Sauna zu sein?
    \item Ist Ihr Mensch frei von körperlichen Beschwerden wie Rücken- oder Schulterschmerzen? Hat er Probleme mit Hormonen oder seinem Stoffwechseln? Klagt er regelmäßig darüber, dass er alt wird und nicht mehr jede Bewegung möglich wäre? Kann Ihr Mensch problemlos normale Portionen Nahrung zu sich nehmen, ohne an Gewicht zuzulegen?
    \item Hat Ihr Zweibeiner ein gesundes Gewicht? Hat er höchstens ein paar Speckröllchen und ansonsten eine athletische Figur? 
    \item Kann Ihr Mensch ohne Probleme eine Wasserkiste oder mit Hilfe eines anderen Menschen einen Schrank schleppen?
    \item Kann Ihr Mensch schwimmen?\footnote{Achtung, übergewichtige Menschen schwimmen von sich aus oben.}
    \item Kann Ihr Mensch ein zehnstöckiges Gebäude nach oben über die Treppe laufen, ohne außer Puste zu kommen?
\end{itemize}

Ist Ihr Mensch noch nicht so weit? Kein Problem, das Buch kann Ihnen weiterhelfen und Ihren Menschen vollumfänglich auf das Leben vorbereiten.  