% !TEX root = menschen-halten.tex
\section{F\&A: Fragen und Aktionen)}
\label{movement-conclusion}

Die Bandbreite menschlicher Leistungen im sportlichen Bereich ist enorm. Der Mensch kann Speere über neunzig Meter weit werfen, fünfhundert Kilogramm vom Boden heben, bis zu vierzig Kilometer pro Stunde schnell sprinten, hundertachtzig Kilometer Fahrrad fahren, zweiundvierzig Kilometer weit rennen, unglaublich koordinierte Bewegungen an den verschiedensten Orten darstellen -- alles mit viel Übung\footnote{Zeit und Geld vorausgesetzt.}, aber fast jeder Mensch könnte sich diese Fähigkeiten in einem gewissen Maß aneignen. 

Von diesen Fähigkeiten macht der Mensch im Alltag praktisch keinen Gebrauch. Die meisten freuen sich bereits, wenn sie an einem Tag in der Woche Sport getrieben haben. Damit vernachlässigen sie nicht nur ihr Potenzial, sondern gefährden auch ihre Gesundheit. Denn die körperliche Leistungsfähigkeit ist auch ein wesentlicher Ausdruck davon, ob sie mit hoher Lebensqualität altern werden. Körperliche Schwächen und Gebrechen sind es, die sie letztlich früher ins Grab bringen und das Risiko für alle modernen Wohlstandkrankheiten erhöhen.

Dabei ist ein bewegungsreiches Leben nicht besonders schwierig. Das Fundament liegt eine Umstellung des Alltags und die richtigen Mitstreiter an der Seite. Die folgenden Fragen und Aktionen geben eine Orientierungshilfe, was die wesentlichen Aspekte von Bewegung sind, basierend auf den vorhergehenden Kapiteln.

\subsection*{Fragen}

\subsubsection*{Ausdauertraining- oder Krafttraining? Was ist mit Koordination oder Yoga...?}

Die beste Form des Sports ist diejenige, die Ihr Mensch macht. Seit Anbeginn der Existenz der Menschheit war das Leben eines Menschen geprägt durch Bewegung. Die Natur unterschied nicht, ob Ihr Mensch Krafttraining oder Ausdauer gemäß Plan zu trainieren hat. 
Damals musste sich ein Mensch bewegen, um zu überleben und der Alltag enthielt alles, was einen Menschen in Bewegung hielt. Die moderne Zeit hat viele dieser positiven Einflussfaktoren aus dem Leben Ihres Menschen genommen, wodurch es überhaupt es wieder notwendig ist, dass ein Mensch Sport betreibt. 
Was es am Ende ist, was Ihr Mensch macht, ist gar nicht so wichtig, solange es nicht stark einseitig ist. Lassen Sie Ihren Menschen nicht nur die ganze Zeit auf einem Laufband laufen und geben Sie ihm nicht nur schwere Gewichte, die er acht bis zwölf Mal pro Set hebt. Implementieren Sie für Ihren Menschen kreativ viele Möglichkeiten der Bewegung. Darf es ein Stehtisch mit Laufband sein oder ein Menschenkind, mit dem Ihr Mensch regelmäßig auf den \hyperref[movement-play]{Spielplatz} geht? Wie wäre es mit entspannenden Yoga-Sessions gepaart mit Holzfällen? Es bleibt Ihnen überlassen, welche Art von Bewegung Ihr Mensch in seinem Leben implementiert, sofern er sich umfassend bewegt. Das wird dafür sorgen, dass Ihr Mensch sich bis in hohe Alter wohlfühlt und gesund ist. 

\subsubsection*{Macht Krafttraining den Körper nicht kaputt?} 

Darauf gibt es eine einfache und gesicherte Wahrheit: Nein. Unter schlechten Umständen, mit schlechter Technik, zu hohem Trainingsgewicht, Vorverletzungen oder schlechter Einweisung besteht das Risiko von Verletzungen. Wenn das Ego aus dem Training weggelassen wird, die Menschen für sich und nicht die Anerkennung anderer Krafttraining betreiben, haben sie einen ausschlaggebenden Mehrwert. \hyperref[movement-strength]{Krafttraining} ist aus unterschiedlichsten Gründen sinnvoll, aber der Vorteil durch die Sicherung der Lebensqualität und Gesundheit ist der wohl wichtigste Grund dafür. Ihr Mensch sollte unter anderem seine Stärke trainieren, um artgerecht zu leben. Bei Zweifeln der Gefahr von Krafttraining ist ein kompetenter Trainer sinnvoll, um jegliches Risiko zu eliminieren\footnote{Wir sind schließlich immer noch im deutschsprachigen Raum, die Menschen brauchen Stabilität und Ordnung.}.

\subsubsection*{Benötige ich Tools für meinen Menschen, damit er sportlich sein kann?}

Die meisten Dinge, die Ihr Mensch benötigt, um Bewegung in den Alltag zu integrieren, dürften bereits in seinem Schrank herumliegen: Je nach Wetter kurze oder lange Kleidung mit ausreichendem Bewegungsumfang, ein paar bequeme Schuhe und das ist es. Ihr Mensch benötigt für den ersten Start keine spezielle Ausrüstung. Je nach Sportart, die Ihr Mensch betreiben möchte, wird zwar in der Theorie weiteres Equipment benötigt, was sich allerdings fast immer ausleihen lässt. 

Vertrauen Sie nicht auf Versprechen von Sportartikelherstellern. Es ist nicht notwendig, spezielle Laufschuhe, Sportkleidung oder gar Pulsfrequenzmessung zu nutzen. Gerade letzteres ist überbewertet. Die Kontrolle der Herzfrequenz beim Sport ist irrelevant. In der Theorie sollte eine Pulsfrequenzmessung \guillemetright optimale\guillemetleft{} Belastung oder sogar Fettabbau gewährleisten. In der Realität ist der Effekt so klein, dass positive Auswirkungen nicht merklich sind. Wer sich und sein Herz ab und zu maximal belastet, spart sich diese unnötige Ausgabe. Der Benefit des gesundheitlich angebrachten Trainingsbereichs ist nur insofern relevant, wenn Ihr Mensch besonders empfindlich beim Herzen ist, beispielsweise nach einem Herzinfarkt oder Schlaganfalls. Hier empfehlen ich Ihnen, auf die Empfehlung eines (Sport-)Arztes zu hören, da dieser passend auf individuelle Bedürfnisse einzugehen vermag.


\subsection*{Aktionen}

Jeder Halter eines Menschen ist irgendwann in seinem Leben ein Anfänger. Wissen Sie noch, als Sie Ihren Menschen bekommen haben? Wussten Sie damals, wie Sie mit ihm umgehen sollen? Mit den Jahren der Pflege und des Umgangs haben Sie gelernt, was die bestimmten Bedürfnisse Ihres Menschen sind und was Ihr Mensch will. 
Die Zeit des Lernens verkürzen Sie unter der Zuhilfenahme eines Coachs\footnote{Dieses Buch ist auch ein Coach. Sie haben mit ihm eine gute Wahl getroffen.}, da dieser bereits die Erfahrungen gemacht hat und das Fachwissen besitzt, um Ihren Menschen vor den großen Fehlern zu warnen, denn er hat sie bereits selbst gemacht. Falls Sie sich Energie und das Geld sparen wollen\footnote{Das Buch haben Sie ja bereits, so viel zum Thema Sparen.}, gibt es ein paar Grundregeln, die Ihren Menschen das Leben erleichtern werden.


\begin{enumerate}
    \item \textbf{Oft und abwechslungsreich bewegen.} Suchen Sie im Alltag Ihres Menschen nach Bewegungsmöglichkeiten. Lassen Sie ihn statt des Lifts die Treppe steigen. Wenn er backt, soll er den Schneebesen nutzen und nicht die Küchenmaschine. Lassen Sie ihn zur Arbeit gehen. Wenn das nicht geht, darf er das Fahrrad nehmen. Wenn das nicht geht, bekommt er ein E-Bike. Auf der Arbeit zu stehen mit einem regelmäßigen Positionswechsel inklusive Bewegung aller Gelenke bringt noch mehr Bewegung in sein Leben. Integrieren Sie kleine Trainingshäppchen in den Alltag. Hier die Treppe nehmen, da fünf Liegestütz, hier über die Mauer springen oder auf den Baum klettern. Die Möglichkeiten sind unbegrenzt und der Spieltrieb Ihres Menschen unterstützt ihn dabei. Je mehr Bewegung im Alltag stattfindet, desto weniger spezifisches Training ist nötig.
    \item \textbf{Kurz intensiv, oft leicht.} Ihr Mensch soll kurzes, knackiges und hochintensives Training mit viel Bewegung kombinieren, die nur ganz leicht und nicht belastend ist. Das holt die beste Anpassungsfähigkeit seines Körpers hervor und ihm wird nicht langweilig. 
    \item \textbf{Sitzen ist böse.} Nehmen Sie Ihrem Menschen den bequemen Stuhl oder die geliebte Couch weg. Ihr Mensch bleibt dadurch viel mehr in Bewegung. Auch im Stehen lassen sich viele Tätigkeiten machen, aber für zwölf Stunden zu sitzen degeneriert seinen Körper.
    \item \textbf{Komfort vermeiden.} Jede Form von Komfort verspricht, das Leben Ihres Menschen leichter zu machen. Leichter ist in einer Zeit, in der alles leicht ist, die denkbar falsche Herangehensweise. Machen Sie Ihrem Menschen das Leben schwer. Sorgen Sie dafür, dass er die Extrameile gehen muss und nicht den ganzen Tag herumliegt, denn er neigt zur Trägheit.
    \item \textbf{Bring deinen Menschen zum Spielen raus.} Früh beginnen heißt frühe Erfolge verzeichnen. Ihr Menschenkind zum Spielen nach draußen zu verfrachten wird ihm helfen, Bewegung, soziale Interaktion, Flexibilität, die Einschätzung der eigenen Fähigkeiten, den Verlust von Ängsten und vielem mehr spielerisch, unterhaltsam und ohne Druck zu lernen. Diese Denkweise überträgt sich auf den späteren Menschen und wird ihm helfen, Bewegung anzugehen und in den Alltag zu integrieren. Spielen Sie mit Ihrem Menschen auch mit zunehmendem Alter, damit er sich aus Freude bewegt. Die positive Konditionierung ist immer noch die beste Form, um Ihr Haustier an Bewegung zu gewöhnen.
    \item \textbf{Jeder sollte funktional trainieren.} Viele Menschen wehren sich gegen Krafttraining, weil sie nicht gern wie Bodybuilder aussehen wollen. Das ist die falsche Sichtweise. Der Körper reagiert so gut auf Krafttraining, dass es hinsichtlich Vermeidung von Verletzung und späterer Lebensqualität wichtig ist, Krafttraining zu betreiben. Es muss nicht viel sein, aber es sollte im Alltag jeder Person vorkommen. Die Form eines Bodybuilders erreicht man ohne illegale Medikamente nicht, auch nicht als Weibchen.
    \item\textbf{Langfristig denken.} Sie wollen für Ihren Menschen keine kurzfristige Umstellung zur Bewegung, sondern eine langfristige Änderung der Gewohnheit zu einem bewegten Leben. Denn das ist die einzige Option, wie Ihr Mensch Gesundheit und Langlebigkeit kombiniert.
\end{enumerate}

\subsection*{Der Weg zum Erfolg}

Ihr Mensch braucht vielleicht einen kleinen Motivationsschub? Erinnern Sie ihn daran, dass große Ziele durch kontinuierliche Anstrengung erreicht werden können. Jeder Schritt nach vorne ist ein Schritt in Richtung Erfolg. Statt sich von der Dauer des Prozesses entmutigen zu lassen, richten Sie den Fokus auf die kleinen Erfolge, die auf dem Weg dorthin liegen. Jeder Fortschritt zählt und baut auf dem vorherigen auf – ein Schritt nach dem anderen.

Dieser Weg ist kein Sprint, sondern ein Marathon, und genau das macht ihn so lohnend. Die Transformation Ihres Menschen wird nicht nur sichtbar sein, sondern auch diejenigen beeindrucken, die ihn auf diesem Weg begleiten. Stellen Sie sich vor, wie stolz Sie und Ihr Mensch sein werden, wenn Freunde und Familie vor Staunen den Atem anhalten.

Übernehmen Sie die Verantwortung und führen Sie Ihren Menschen mit Zuversicht. Die Zeit des Stillstands und der Unsicherheit ist vorbei. Jetzt beginnt die Phase der Aktivität, des Wachstums und der Stärke. Gemeinsam können Sie Hindernisse überwinden und ein neues Kapitel der Energie, Disziplin und Lebensfreude aufschlagen. Sie sind die treibende Kraft, die Ihren Menschen begleitet und unterstützt. Vertrauen Sie auf Ihren Einfluss – Sie haben die Fähigkeit, ihn zu motivieren und zu inspirieren!

\subsection*{Sportliche Ziele für jeden Menschen}

Jeder gesunde Mensch kann gewisse sportliche Ziele erreichen. Wir stellen Ihnen Ziele vor, die sich jeder vornehmen kann und sehr wahrscheinlich erreicht\footnote{Ein bisschen Verlust gibt es immer. Nicht jeder ist perfekt gesund und gleich leistungsfähig. Jemandem im Rollstuhl sollten Sie beispielsweise keine Kniebeugen vorschlagen.}. Alles darüber hinaus ist ein Bonus.

\begin{itemize}
    \item Fünf Kilometer schnell laufen
    \item Zehn Kniebeugen mit dem 1,5-fachen Körpergewicht
    \item Einmal Kreuzheben mit dem zweifachen Körpergewicht
    \item 30 Liegestütze 
    \item 10 Klimmzüge
    \item 20 Dips
    \item Überkopfdrücken mit dem Körpergewicht
    \item Einen Kilometer schwimmen
    \item 100 Seilsprünge
\end{itemize}