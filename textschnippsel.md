Die Basis für ein gutes Leben beginnt an der Stelle, die am leichtesten greifbar ist, seinem Körper. Viele Bereiche haben auf den menschlichen Körper starken Einfluss. Wenn wir an unsere Gesunderhaltung denken, fallen uns primär oft Essen und Bewegung ein. Das sind die Themen, mit denen wir immer wieder konfrontiert werden. Sie sind naheliegend, weil ihr Mensch jeden Tag isst und sich bestenfalls bewegt. Andere Bereiche verbleiben eher im Hintergrund. Die menschliche Umgebung zeigt mit Licht, Temperatur oder dem Schutz vor Umwelteinflüssen ebenso Wirkung wie auch gesellschaftliche Rahmenbedingungen oder seine innere Einstellung. Wir zeigen Ihnen, warum auch diese Aspekte wichtig sind und mehr Auswirkungen haben, als Sie zu wissen glauben.

Essen jagen, Obst pflücken, tanzen, musizieren, Geräte erfinden, Sport treiben, sich hochgradig vernetzen, sich paaren, Bauwerke errichten, Bücher schreiben, Brote backen, arbeiten gehen, auf dem Sofa sitzen, das alles ist typisch Mensch. 

Der \textit{Hund} ist das Tier schlechthin, das sich mit dem Menschen zusammen entwickelt hat und die gemeinsame Evolution ist wichtiger Bestandteil eines glücklichen Zusammenlebens.

Wir merken uns: Eine zu geringe Dosis hat keine Wirkung. Wenn Sie nun den Reiz verstärken, hat der Mensch noch die Fähigkeit, sich anzupassen. Diese Fähigkeit nimmt mit erhöhter Reizstärke wieder ab. Ab einer gewissen Schwelle nullt sich die vorteilhafte Anpassung, da der menschliche Körper zu sehr damit beschäftigt ist, den Schaden zu reparieren und über keine Ressourcen verfügt, um sich positiv zu entwickeln (weil der Reiz zu stark ist). Übertritt der Reiz diese Schwelle, schädigen Sie den Menschen. Das kann zu Verletzungen, Krankheiten oder bis zu seinem Tod führen.




\subsubsection*{Das böse Fleisch}



Umgekehrt funktioniert die Argumentation auch nicht. Wenn sich jemand besser fühlt, weil er den Anteil an Gemüse und Obst in seiner Ernährung mit der Umstellung auf Veganismus drastisch erhöht, ist das vorbildlich. Keine Frage, Obst und Gemüse sind gesund und werden es immer bleiben. Schließlich sind Pflanzen bereits seit Anbeginn der Zeit Bestandteil in unserer Ernährung und sollten immer verzehrt werden. Die Kosten einer vollständig veganen Ernährungsweise zeigen sich allerdings erst später, wenn die Nährstoffspeicher verarmen: Keine Energie, Depressionen, schlechte Haut, schlechtes Inmmunsystem, Vergesslichkeit und zu guter Letzt eine fehlende Libido machen Menschen mit den besten Vorsätzen die Hölle heiß.


Was auf der einen Seite natürlich Aufmerksamkeit, auch außerhalb der Filterblase, schafft, erzeugt genauso eine Gegenbewegung. Plötzlich ist eine karnivore Diät der heißeste Shit. Guter Plan, noch einen Teil der Nahrung auf das gesamte Leben projezieren und eine weitere Bewegung zu schaffen, die die andere Seite des Extrems ausreizt.


\subsubsection*{Aber die Massentierhaltung, den Kühen werde ihre Babys
weggennommen, bla bla
bla}

Zwischen zwei verschiedenen Dingen unterscheiden wir:

\begin{itemize}
   \item Was der Körper braucht
   \item Welche Auswirkungen Fleisch auf die Umwelt und die Tiere hat
\end{itemize}



Wir sehen hier, dass widersprüchliches Verhalten ein Teil des Menschen ist, was auch seinen Reiz ausmacht.


\subsection*{Bad boy}

Wenn Ihr Mensch ein böser Junge (oder ein böses Mädchen) war, kommt es auf die richtige Verhaltensweise an, um das Verhalten zu korrigieren. 

\begin{itemize}
    \item \textbf{Die Gelegenheit nutzen.} Sobald Ihr Mensch unpassendes Verhalten an den Tag legen, korrigieren Sie ihn direkt und schnell. Wenn Sie zu lange warten, kann er es nicht mit dem Fehlverhalten in Verbindung bringen, was nur der Beziehung schade. Zu lange zu warten, um keinen Schaden anzurichten oder andere Hintergedanken helfen dabei nicht. 
    \item \textbf{Nicht nachtragend.} Seien Sie nicht nachtragend. Auch wenn Sie das Verhalten Ihres Menschen stört, es ist geschehen. Sie haben vollkommen in der Hand, wie Sie damit umgehen und welche Konsequenzen Sie ziehen.
    \item \textbf{Angemessen.} Reagieren Sie entsprechend der Situation. Wenn Ihr Mensch auf Diät einen Schokoriegel heimlich gefuttert hat und Sie sich aufregen, ist das überzogen. Vielleicht war es Ihr Fehler und die falsche Herangehensweise? Ist er allerdings Ihnen gegenüber handgreiflich geworden, ist eine beherzte Faust eventuell das Mittel der Wahl.
    \item \textbf{Ruhe bewahren.} Bleiben Sie stets ruhig und gelassen. In der Ruhe liegt die Kraft und nur wer ruhig bleibt, kann überlegt und besonnen handeln. Keine wütende Person hat jemals gute Entscheidungen getroffen. 
\end{itemize}
