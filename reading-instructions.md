# Anleitung zum Probelesen von - Menschen halten -

Liebe Korrekturleserin, lieber Korrekturleser,

danke, dass du dich der Mammutaufgabe annimmst und mein Buch Probe liest. Mit etwas Glück wird das Werk zu einem Bestseller und mit deinem Ehrenplatz im Dankestext. Ich habe mir einige wenige Regeln überlegt, die es mir helfen, ein noch besseres Werk zu erzeugen. Du hast die Macht!

Hier sind die Regeln:
1. *Streichen vor Hinzufügen*: Wenn du einen Text liest und dir gefällt ein Abschnitt, ein Satz, ein Wort, eine Überschrift oder was auch immer nicht, überlege dir, was du wegstreichen willst. Nur wenn wirklich etwas fehlt oder anderes erklärt werden muss, soll Text hinzugefügt werden.
2. *Sei radikal*: Egal, was dich stört, markiere und kommentiere es. Nimm keine Rücksicht auf die Gefühle des Autors, er wird es schon aushalten.
3. *Gegen Unstimmigkeiten*: Ist im Buch etwas in seinem Konzept, dem Aufbau nicht stimmig, sollte umgestellt werden oder ganz anders. Diese Kommentare sind sehr wichtig, auch wenn alles ändern.
4. *Zeitlos*: Stell dir vor, dass das Buch noch in 100 oder 1.000 Jahren gelesen wird. Welche Abschnitte sollten dafür enfernt oder geändert werden?
5. *Hätte, hätte*: Passivkonstruktionen wie "hätte", "können", "würden" oder vergleichbar sollen weg. Bitte markieren :)

Das wars schon. Danke fürs Lesen!

Liebe Grüße
Niklas, le auteur 