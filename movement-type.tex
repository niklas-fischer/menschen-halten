% !TEX root = menschen-halten.tex
\section{Schleppen, kämpfen, Hindernisse überwinden}
\label{movement-type}

Bewegung ist für den Menschen ein essenzieller Bestandteil zur Erhaltung der Gesundheit. Für den ambitionierten Halter stellt sich die Frage, welche Art von Bewegung gesund ist. Welche Bewegungen sind für unseren Menschen förderlich, welche erhöhen seine Verletzungsgefahr? Was empfehlen wir unserem Menschen und was wollen wir anregen, dass er es vermehrt macht? 

Erste Orientierungspunkte geben uns die Art von Bewegung, wie sie im Alltag von Jägern und Sammlern vorkam. Der Alltag war vor allem abwechslungsreich. Wenn es etwas an Bewegung zu meiden gibt, dann sind es monotone Bewegungsabläufe, wie man sie aus Fließbandarbeit kennt. Immer wieder dieselbe Bewegung, welche Stunden über Stunden ausgeführt wird, stärkt den Körper nicht, sondern lässt ihn nach und nach degradieren. Dementsprechend ist es vor allem die Abwechslung, die wir in der Bewegung suchen sollten. Zu vermeiden gilt die Sesshaftigkeit, gleichbleibende Arbeit an Maschinen oder jede andere Art der Bewegung, die sich dauerhaft wiederholt. Geben Sie Ihrem Menschen den Freiraum, sich viel und divers zu bewegen. Untersuchen Sie genau sein Terrarium. Hat Ihr Mensch ausreichend Platz, sodass er sich bewegen will? Hat er Bewegungsmöglichkeiten wie Sportgeräte in der Nähe? Bewegt sich Ihr Mensch aufgrund seines Alltags nur sehr wenig\footnote{Seien Sie hier sehr wachsam. Viele Menschen neigen dazu, sich sehr wenig zu bewegen, weil sie ständig arbeiten.}?

Suchen wir unserem Menschen eine neue Sportart, die ihn das Leben lang begleiten soll, hilft es, sich an natürlichen Bewegungsabläufen zu orientieren. 

\subsection*{Willkommen im Zeitalter der Bewegung}

Sowohl die menschlichen Vorfahren als auch indigene Völker hatten tagtäglich mit so einigen Strapazen zu kämpfen. Die Nahrungssuche war mit Gefahren verbunden, die in Zeiten von Autos und Supermärkten schnell vergessen sind. Hinter dem Gebüsch konnte ein wildes Tier lauern, das man aus Versehen beim Fressen überrascht hat oder das seine Jungen beschützen will \footnote{Menschen stehen nicht auf der Liste der herkömmlichen Nahrungsmittel von Tieren. Wenn ein Mensch von einem Tier gefressen wurde, dann sollte es ursprünglich nicht gefressen werden. Das Tier hat seine Nachkommen beschützt, den Menschen mit Nahrung verwechselt oder war aus anderen Gründen aggressiv.}.
Auch sonst war der Alltag gefährlich. Eine Verletzung konnte einen außerhalb des Lagers gehunfähig machen. Eine Infektion nach einer Verletzung war häufig ein zäher Kampf und konnte tödlich ausgehen. 

\subsection*{Run, bitch, run}

Dem konnte man nur gewachsen sein, wenn der Körper zu jeder Zeit und in jedem Alter mitspielte. Denn bis zum fortpflanzungsfähigen Alter und bis der Nachwuchs schließlich da war, war das Ziel überleben.
Zum Überleben war ein notwendiges Mittel die Jagd. Diese erfordert besondere Fähigkeiten. Der Mensch hat ein langes Durchhaltevermögen, was das Zurücklegen von langen Wegen betrifft. Kaum ein anderes Lebewesen kann jeden Tag locker zwanzig bis dreißig Kilometer zurücklegen ohne Anstrengung. Hat er erst Jagdbeute entdeckt und sich angeschlichen, bestand das Risiko, dass sie ihn entdeckte und der Jagderfolg zunichte war, wenn er nicht sofort reagierte. Dann musste der Mensch seiner Beute hinterherrennen. Dazu sind Sprints essenziell. Eine kurze Distanz konnte er so schnell zurücklegen, um entweder seine Beute zu jagen oder auch vor gefährlichen Situationen zu entfliehen. Von seiner Fähigkeit zu rennen hing sein Überleben ab. 

Wer nicht fliehen konnte, wurde von einem Raubtier zerfleischt. Während einer Flucht musste er schnell sein, Hindernisse überwinden durch Sprünge und uns schnell in einer unbekannten Umgebung zurechtfinden. Heutzutage ist die menschliche Umgebung so aufgebaut, dass sich jeder einfach zurechtfindet und wenig Gefahrenpotenzial besteht. Wo gefährliche Straßenecken mit hoher Unfallgefahr war, werden Schilder montiert, um zukünftige Unfälle zu vermeiden. Hinter kaum einer Ecke lauert noch ein gefährliches, wildes Tier. Das gefährlichste Tier, was ihm heute in einer Stadt begegnet, ist eine Spinne\footnote{Die Tiere selbst sind ungefährlich, aber die blinde Panik vor dem Tier könnte eine Gefahr sein}.

\subsection*{Den Einkauf tragen}

Zurück zu den Jägern. Nach erfolgreicher Jagd musste die oftmals schwere Jagdbeute \footnote{Menschen haben besonders gerne große Säugetiere gejagt. Sie liefern viel Energie mit vergleichsweise moderatem Jagdaufwand. Dieses Spiel konnte so lange betrieben werden, wie große Jagdbeute zur Verfügung stand}, wieder in das Lager gebracht werden. Dazu war es notwendig, stark und ausdauernd zu sein. Es benötigt muskulöse, ausdauernde Arme und Schultern und einen stabilen Brust- sowie Rückenbereich, um schwere Lasten über lange Wege zu schleppen. Mit etwas Übung ist es realistisch, dass Menschen über einen kurzen Zeitraum Lasten bewegen, die bis zu ein zweifaches des Körpergewichts betragen. 

Mit den Affen teilt der Mensch gemeinsame Vorfahren. Von diesen sind die Fähigkeiten zum Klettern erhalten geblieben. Andere Fähigkeiten, wie der aufrechte Gang, haben sich hingegen im Vorfahren des Menschen perfektioniert. So verhielt es sich auch mit anderen Arten der Bewegung. Der Alltag forderte ein gewisses Maß an Stärke, Ausdauer, Kraftausdauer, Koordination, Mobilität oder Konzentrationsfähigkeit, um nicht von einem wilden Tier angegriffen zu werden oder zu verhungern. Die Fähigkeiten, die damals notwendig waren, sind diejenigen, bei denen sich der Körper schnell und zielgerichtet anpasst. Sobald der Mensch sich mit seinen körperlichen Fähigkeiten auf eine bestimmte Art und Weise spezialisiert, beispielsweise durch seine Entscheidung, nur noch schwere Gewichte zu haben, profitiert er von der starken Anpassungsfähigkeit seines Körpers auf neue Situationen, reduziert damit jedoch seine Flexibilität, mit mehr Situationen als einem schweren Einkauf umzugehen.

Der Mensch ist demnach mehr Generalist als Spezialist. Das ist, was ihn von allen anderen Lebewesen unterscheidet. Er ist nicht in einer Disziplin besonders gut. Dafür kann er sehr vieles so gut, dass er insgesamt einen großen Vorteil gegenüber anderen Lebewesen besitzt. An seiner Natur sollten wir uns orientieren, wenn wir ihn trainieren.

\subsection*{Natürliche Bewegungsabläufe implementieren}

Im Alltag eines Menschen treten verschiedenste Bewegungen auf, für die sein Körper gewappnet sein muss, wenn er verletzungsfrei und stark bleiben will. Dies schließt Bewegungen ein, die in einem natürlichen Alltag eines Menschen vorkommen, nicht nur in der künstlichen Umgebung von heute, in der Bewegung der Bequemlichkeit untergeordnet wird. Die Bewegungen haben vergleichbare und auf den Alltag übertragbare Trainingsübungen.

\textbf{(An-)Heben von Gegenständen.} Ob ein Korb voller gesammelter Früchte oder ein Holzstamm, immer wieder muss der Mensch Gewichte tragen. Eine ähnliche Übung ist im Bereich des Krafttrainings das \textit{Kreuzheben}, wodurch der Mensch seine gesamte Körperkraft enorm steigern kann. Das Ziel sollte es sein, dass Ihr Mensch sein zweieinhalbfaches Körpergewicht anhebt.

\textbf{Tragen von Gegenständen.} Der Mensch hat einen Baumstamm angehoben. Doch was jetzt? Wie bei einem Umzug muss der Gegenstand von einem Ort zum nächsten bewegt werden. Will er diese Übung gesondert trainieren, eignen sich dafür vor allem \textit{Loaded carries}, also das Schleppen von schweren Gewichten über eine festgelegte Distanz. 

\textbf{Sich an etwas hochziehen.} Jeder Mensch hat die Fähigkeiten, sich an etwas hochzuziehen, die meisten haben sie nur nicht mehr geübt. Bringen Sie Ihren Menschen zum \textit{Klettern} oder lassen Sie ihn wenigstens ein paar \textit{Klimmzüge machen}, um diese Fähigkeit zu trainieren.

\textbf{Verfolgen von Jagdbeute.} Ohne Nahrung zwischen den Beissern war schnell Schicht im Schacht. Der Mensch muss situationsabhängig schnell sprinten, damit ihm sein Futter nicht entwischt. Durch \textit{Sprints} oder längeres \textit{Rennen} trainieren Sie die Ausdauer und Schnelligkeit Ihres Menschen. 

\textbf{Überwinden von Hindernissen.} Was hat ein Mensch gemacht, wenn er im Wald hinter einem Hirsch gerannt ist und ein Strauch im Weg war? Sicher ist er nicht direkt dagegen gelaufen, sondern hat das Hindernis elegant überwunden. Außerhalb des Waldes in Städten sind die Hindernisse immer rarer, weshalb sich ein entsprechendes Training wie \href{https://de.wikipedia.org/wiki/Parkour}{\textit{Parkour}} bewährt hat, um Ausdauer und Koordination zu erlangen.

\textbf{(Straßen-)Kämpfe.} Wer nicht kämpfen will, rennt. Wer kämpfen muss, ist bestens beraten, auch kämpfen zu können. Immer wieder kann es vorkommen, dass ein Mensch in einem Kampf gerät. Das erste Mittel der Wahl ist immer die Flucht, doch wenn keine Flucht möglich ist, sind \textit{Kampfsport} oder \textit{-kunst} geeignet, Kraftausdauer, Koordination und Selbstverteidigungsfähigkeiten auszubauen. 

\textbf{Tanz und Musik.} Bewegung war nicht nur ein Muss, sondern auch eine Freude. Durch \textit{Tanz} bewegen sich Menschen bis zur Ekstase, durch \textit{Instrumente spielen} brachten Sie Spaß und Freude in gesellschaftliche Zusammenkünfte. Sein Gehirn bedankt sich, denn sowohl Tanzen als auch Musikinstrumente sind ein ausgezeichnetes Training für den Geist. 
 