#!/bin/bash

# Pulling all files from git dir
echo "Pulling all files from git directory..."
git pull

# Directory for HTML output
dir="pages/"
  
### Check for dir, if not found create it using mkdir 
[ ! -d "$dir" ] && mkdir -p "$dir"

# Removing old files in publishing dir
echo "Checking for files in ${dir} and deleting them if any..."
shopt -s extglob
files=($dir*)
if [ ${#files[@]} -gt 0 ]; then git rm -r $dir*; fi

# Using BookML to create HTML site
echo "Creating HTML site..."
latexml menschen-halten.tex | latexmlpost - \
    --format=html5 \
    --destination=pages/index.html \
    --splitat=chapter \
    --splitnaming=label \
    --navigationtoc=context 

# Changing directory to $dir
cd $dir

git_sync () {
    # Add all files in submodule
    echo "Staging files in $dir submodule..."
    git add $1

    # Commit all created files
    echo "Committing HTML files..."
    git commit -m "$dir: New HTML version"

    # Push all files into submodule repo
    echo "Pushing files to submodule..."
    git push
}

# Syncing files from submodule to repository
git_sync .

# Changing directory to main directory
cd ..

# Syncing main files to repository
git_sync $dir

# That's it
echo "Finished! Woob woob"