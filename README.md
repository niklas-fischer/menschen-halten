<div align="center">

# Menschen halten für Anfänger
## Artgerechte Haltung der verbreitetsten Hominiden-Spezies

[![Licence badge](https://img.shields.io/static/v1?label=creative%20commons&message=BY-NC-SA&color=black&labelColor=lightgrey&style=for-the-badge)](LICENCE.md)
[![Codeberg Badge](https://img.shields.io/badge/Codeberg-2185d0?style=for-the-badge&logo=codeberg&logoColor=white)](https://codeberg.org)
![Reading level badge](https://img.shields.io/static/v1?label=Reading%20Level&message=1st%20grade&labelColor=ffbb33&color=ff8800&style=for-the-badge)


*Menschen halten für Anfänger* is a human husbandry guidebook, inspired by animal husbandry books and all the boring guidebooks about sports, nutrition, environment and so on.

The book teaches you how to raise a human according to proven principles and what rules of thumb and methods to follow to help your human live a happy and content life.

The book is written in German 🇩🇪

[Intro](#📖-intro) •
[Featuring](#👇-featuring) •
[Featuring](#🗺️-roadmap) •
[Licence](#🔖-licence) •
[Installation](#🖥️-installation) •
[Support me](#♥️-show-your-support) •
[Contact](#🤙-get-in-contact-with-the-author)


</div>

# 📖 Intro

Congratulations on your decision to offer a human a place in your life! Look forward to many great experiences and the long time you may spend together with your two-legged friend. But the life of a human owner does not only consist of playing and cuddling: training, education as well as a species-appropriate diet also play a major role. In order to answer all important questions in this regard, this guidebook was written especially for you. 


<p align="right">(<a href="#top">back to top</a>)</p>

# 👇 Featuring 
In this book, you can expect the following contents, among others:

- Toys to entertain your human
- Species appropriate nutrition for your human
- How to get a healthy human
- Initial equipment
- Movement practice

<p align="right">(<a href="#top">back to top</a>)</p>

# 🗺️ Roadmap

- [x] Book website
- [x] Automating HTML site publishing
- [ ] Author website/blog (WIP)
- [ ] Evaluation of manuscript
- [ ] Book cover design
- [ ] ISBN registration
- [ ] Copy editing
- [ ] Professional typesetting
- [ ] Publish eBook
- [ ] Specimen book copy
- [ ] Printing/publishing (eBook)
- [ ] Getting book available at book seller


<p align="right">(<a href="#top">back to top</a>)</p>

# 🔖 Licence 

**My content is licensed under** [**CC BY-NC-SA 4.0**](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Copy it, tattoo it, paint it on an airplane, print it on posters, read it to your kittens to put them to sleep - anything goes. 
The only thing that matters is that you mention my name, refer to the license and indicate changes. 
Furthermore, only I am allowed to use the content commercially. You also have to publish the modified data under the same license. 

<p align="right">(<a href="#top">back to top</a>)</p>


# 🖥️ Getting started

To get a local copy up and running follow these simple steps.

## Prerequisites

1. Go to your working directory and clone the repository

```console
i@bims:~$ git clone --recursive https://codeberg.org/niklas-fischer/menschen-halten.git
```
2. Navigate to the newly created directory `menschen-halten` with:

```console
i@bims:~$ cd menschen-halten/
```

Open the files with a code editor of your choice. To make changes to it and create a new PDF, you need to install LaTeX, see below. 

Click here for the installation with [Windows](##-installation:-windows). Click here for the installation on [.deb-based Linux distributions](##-installation:-deb). 
Click here for the installation on [.rpm-based Linux distributions](##-installation:-rpm). 

3. Initialize BookML with

```console
i@bims:~$ cd bookml/ && make
```

### Installation: Windows

Download and install [MikTeX](https://miktex.org/download). All necessary packages will be downloaded and installed after confirmation.

### Installation: DEB

```console
i@bims:~$ sudo apt-get install texlive-base texlive-lang-german biber latexmk texlive-latex-recommended texlive-latex-extra
```

### Installation: RPM

```console
i@bims:~$ sudo dnf install texlive-scheme-basic latexmk texlive-microtype "texlive*german" texlive-mlmodern texlive-xcolor texlive-biblatex

```

## Export as a HTML site

If you want to export the book directly as HTML page, all necessary packages are already included. Run this bash script:

```console
i@bims:~$ ./publi.sh
```

The script generates the necessary HTML files and publishes them in the `pages` submodule using git.

<p align="right">(<a href="#top">back to top</a>)</p>

# ♥️ Show your Support

I love it when people read my book and share its content. If you like this book and want to support me in writing more awesome stuff for you, I’d really appreciate a donation. 

**PayPal**:

[![PayPal Badge](https://img.shields.io/badge/PayPal-003087?logo=paypal&logoColor=white&style=for-the-badge)](https://www.paypal.com/paypalme/ichbinniklasfischer)


<p align="right">(<a href="#top">back to top</a>)</p>
    
# 🤙 Get in contact with the author

Have questions about the book or need help? Do not hesitate to contact me. I would love to hear from you 🙂

[![Telegram Badge](https://img.shields.io/badge/Telegram-blue?logo=telegram&logoColor=white&style=for-the-badge)](https://t.me/niklas_fischer)


<p align="right">(<a href="#top">back to top</a>)</p>



